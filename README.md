<h1>Guia de Referência para Docker</h1>

<img src='a.png' alt=''>
<p>Este é um guia rápido que resume os comandos básicos do Docker para facilitar o uso da plataforma de contêineres.</p>

 <h2>Instalação do Docker</h2>
    <p>Para instalar o Docker, siga as instruções oficiais de acordo com o seu sistema operacional:</p>
    <ul>
        <li><a href="https://docs.docker.com/engine/install/">Instalação do Docker no Linux</a></li>
        <li><a href="https://docs.docker.com/desktop/install/">Instalação do Docker no Windows</a></li>
        <li><a href="https://docs.docker.com/desktop/install/">Instalação do Docker no macOS</a></li>
    </ul>

 <h2>Comandos Básicos do Docker</h2>
    <ol>
        <li><strong>Verificar a Versão do Docker</strong></li>
        <pre><code>docker --version</code></pre>

  <li><strong>Verificar Informações do Docker</strong></li>
        <pre><code>docker info</code></pre>

  <li><strong>Baixar uma Imagem do Docker Hub</strong></li>
        <pre><code>docker pull NOME_DA_IMAGEM[:TAG]</code></pre>

 <li><strong>Listar Todas as Imagens Baixadas</strong></li>
        <pre><code>docker images</code></pre>

   <li><strong>Criar um Contêiner a partir de uma Imagem</strong></li>
        <pre><code>docker run NOME_DA_IMAGEM[:TAG]</code></pre>

   <li><strong>Listar Todos os Contêineres em Execução</strong></li>
       <pre><code>docker ps</code></pre>

   <li><strong>Listar Todos os Contêineres (incluindo os parados)</strong></li>
        <pre><code>docker ps -a</code></pre>

   <li><strong>Parar um Contêiner em Execução</strong></li>
        <pre><code>docker stop ID_DO_CONTAINER</code></pre>
    <li><strong>Iniciar um Contêiner Parado</strong></li>
        <pre><code>docker start ID_DO_CONTAINER</code></pre>

 <li><strong>Remover um Contêiner Parado</strong></li>
        <pre><code>docker rm ID_DO_CONTAINER</code></pre>

   <li><strong>Remover um Contêiner em Execução</strong></li>
        <pre><code>docker rm -f ID_DO_CONTAINER</code></pre>

  <li><strong>Ver Logs de um Contêiner</strong></li>
        <pre><code>docker logs ID_DO_CONTAINER</code></pre>
  <li><strong>Executar um Comando Dentro de um Contêiner em Execução</strong></li>
        <pre><code>docker exec -it ID_DO_CONTAINER comando</code></pre>

 <li><strong>Ver Uso de Recursos dos Contêineres</strong></li>
        <pre><code>docker stats</code></pre>

 <li><strong>Parar Todos os Contêineres em Execução</strong></li>
        <pre><code>docker stop $(docker ps -aq)</code></pre>

  <li><strong>Remover Todos os Contêineres Parados</strong></li>
        <pre><code>docker rm $(docker ps -aq)</code></pre>

  <li><strong>Remover Todas as Imagens Baixadas</strong></li>
        <pre><code>docker rmi $(docker images -q)</code></pre>
    </ol>

 <h2>Conclusão</h2>
    <p>Esse guia cobre os comandos mais comuns do Docker para ajudar você a começar a trabalhar com contêineres. Para mais detalhes sobre cada comando, consulte a <a href="https://docs.docker.com/">documentação oficial do Docker</a>.</p>
 <h1>Guia para Dockerfile e Docker Compose</h1>

  <h2>Criando um Dockerfile</h2>
    <p>O Dockerfile é um arquivo de configuração usado para definir como um contêiner Docker deve ser construído. Aqui está um exemplo básico de como criar um Dockerfile:</p>
    <pre><code># Use uma imagem de base
FROM ubuntu:latest

# Instale pacotes necessários
RUN apt-get update && apt-get install -y \
    nginx \
    && rm -rf /var/lib/apt/lists/*

# Copie arquivos de configuração
COPY nginx.conf /etc/nginx/nginx.conf

# Expõe a porta 80
EXPOSE 80

# Comando padrão para executar quando o contêiner for iniciado
CMD ["nginx", "-g", "daemon off;"]</code></pre>
    <p>Este exemplo cria um contêiner usando a imagem do Ubuntu, instala o servidor web Nginx, copia um arquivo de configuração personalizado e expõe a porta 80. Por fim, define o comando padrão para iniciar o servidor Nginx quando o contêiner for iniciado.</p>

    <h2>Criando um arquivo docker-compose.yml</h2>
    <p>O Docker Compose é uma ferramenta para definir e executar aplicativos Docker de vários contêineres. Ele usa um arquivo YAML para configurar os serviços do aplicativo. Aqui está um exemplo básico de como criar um arquivo docker-compose.yml:</p>
    <pre><code>version: '3'

services:
  web:
    build: .
    ports:
      - "8080:80"
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
    restart: always</code></pre>
    <p>Neste exemplo, definimos um serviço chamado "web" que usa a configuração definida no Dockerfile atual (indicado pelo ponto '.'), mapeia a porta 8080 do host para a porta 80 do contêiner, monta o arquivo de configuração do Nginx e define para reiniciar sempre que o contêiner parar.</p>

 <h2>Conclusão</h2>
    <p>Esses são os conceitos básicos para criar um Dockerfile e um arquivo docker-compose.yml. Com esses arquivos, você pode definir a configuração e os serviços necessários para seus aplicativos Docker. Para mais detalhes sobre Dockerfile e Docker Compose, consulte a <a href="https://docs.docker.com/">documentação oficial do Docker</a>.</p>